/* Nama Program : operator.pas
   Tgl buat     : 3 Oktober 2023
   Deskripsi    : pengoperasian variabel bertipe dasar
   --------------------------------------------------- */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

int main()
{
    system("clear");

    bool TF = false;
    int i = 0, j = 0;
    float x = 0.0, y = 0.0;

    // proses boolean
    printf("tabel kebenaran\n");
    printf("----------------------------------------\n");
    printf("|   x1  |   x2  | x1 and x2 | x1 or x2 |\n");
    printf("----------------------------------------\n");
    printf("| true  | true  | %-9s | %-8s |\n", (true && true) ? "true" : "false", (true || true) ? "true" : "false");
    printf("| true  | false | %-9s | %-8s |\n", (true && false) ? "true" : "false", (true || false) ? "true" : "false");
    printf("| false | true  | %-9s | %-8s |\n", (false && true) ? "true" : "false", (false || true) ? "true" : "false");
    printf("| false | false | %-9s | %-8s |\n", (false && false) ? "true" : "false", (false || false) ? "true" : "false");
    printf("----------------------------------------\n\n");

    // proses operasi numerik
    i = 5;
    j = 2;
    printf("operasi numerik pada tipe data integer\n");
    printf("---------------------------\n");
    printf("| operasi | hasil operasi |\n");
    printf("---------------------------\n");
    printf("| %d + %d   | %7d       |\n",i,j,(i+j));
    printf("| %d - %d   | %7d       |\n",i,j,(i-j));
    printf("| %d * %d   | %7d       |\n",i,j,(i*j));
    printf("| %d / %d   | %10.2f    |\n",i,j,((float) i/j));
    printf("| %d div %d | %7d       |\n",i,j,(i/j));
    printf("| %d mod %d | %7d       |\n",i,j,(i%j));
    printf("---------------------------\n\n");

    x=5.0;
    y=2.0;
    printf("operasi numerik pada tipe data float\n");
    printf("---------------------------------\n");
    printf("|    operasi    | hasil operasi |\n");
    printf("---------------------------------\n");
    printf("| %4.2f + %4.2f   | %9.2f     |\n",x,y,(x+y));
    printf("| %4.2f - %4.2f   | %9.2f     |\n",x,y,(x-y));
    printf("| %4.2f * %4.2f   | %9.2f     |\n",x,y,(x*y));
    printf("| %4.2f / %4.2f   | %9.2f     |\n",x,y,(x/y));    
    printf("---------------------------------\n\n");
    
    // operator relasi numerik 
    TF = x!=y;
    printf("5.0 != 2.0 adalah = %s\n",TF ? "true" : "false");
    TF = x<y;
    printf("5.0 < 2.0 adalah = %s\n",TF ? "true" : "false");

    return 0;
}