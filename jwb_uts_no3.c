// Online C compiler to run C program online
#include <stdio.h>
#include <math.h>
#include <stdbool.h>

int main() {
    // Write C code here
    int N=0;
    int jml_baris=0;
    int jml_kolom=0;
    int ttk_tengah=0;
    
    printf("Masukan N : ");scanf("%d",&N);

    jml_baris = (N*2)-1;
    jml_kolom = jml_baris;
    
    ttk_tengah = ceil(jml_baris/2);

    for(int i=0;i<jml_kolom;i++)
    {
       for(int j=0;j<jml_baris;j++)
       {
          int bts_bawah = 0;
          int bts_atas  = 0;
          
          if(i<=ttk_tengah)
          { 
             bts_bawah = ttk_tengah-i;
             bts_atas  = ttk_tengah+i;
          }else{
             bts_bawah = i-ttk_tengah;
             bts_atas  = jml_kolom-(i-ttk_tengah);
          }
                 bool ctk_spasi=true;
                 for(int k=bts_bawah;k<=bts_atas;k+=2)
                 {
                   if(j==k)
                   {        
                    printf("*");
                    ctk_spasi=false;
                   }
                 }
                 if(ctk_spasi)
                 {
                   printf(" ");     
                 }

       }
       printf("\n");
    }

    return 0;
}

/*

Masukan N : 5
*    
   * *   
  * * *  
 * * * * 
* * * * *
 * * * * 
  * * *  
   * *   
    *    

*/