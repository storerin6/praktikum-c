/*
   Nama Program : masukAbj.c
   Tgl buat     : 10 Oktober 2023
   Deskripsi    : memeriksa inputan
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
  system("clear");

  char A = ' ';

  printf("Masukkan Suatu Karakter : ");scanf("%c",&A);

  if (A>='A' && A<='Z')
  { 
    printf("Anda menekan huruf besar\n");
    printf("Huruf yang anda tekan, Huruf %c\n",A);
  }else
   {
    printf("Anda tidak menekan huruf besar\n");
    printf("Karakter yang anda tekan, Karakter %c\n",A);
   }

  return 0;
}