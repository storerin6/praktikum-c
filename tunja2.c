/*
   Nama Program : tunja2.c
   Tgl buat     : 10 Oktober 2023
   Deskripsi    : menghitung besarnya jumlah tunjangan dan
                  potongan
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    system("clear");

    int JumlahAnak=0;
    float GajiKotor=0.0,Tunjangan=0.0,PersenTunjangan=0.0,PersenPotongan=0.0;
    float Potongan=0.0;

    PersenTunjangan=0.2;
    PersenPotongan=0.05;
    printf("Gaji Kotor ? ");scanf("%f",&GajiKotor);
    printf("Jumlah Anak ? ");scanf("%d",&JumlahAnak);
    if(JumlahAnak>2)
    {
      PersenTunjangan=0.3;
      PersenPotongan=0.07;
    }
    Tunjangan = PersenTunjangan*GajiKotor;
    Potongan = PersenPotongan*GajiKotor;
    printf("Besar Tunjangan = Rp %10.2f\n",Tunjangan);
    printf("Besar Potongan = Rp %10.2f\n",Potongan);

    return 0;
}