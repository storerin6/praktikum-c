/*
   Nama Program : masukA.c
   Tgl buat     : 10 Oktober 2023
   Deskripsi    : memeriksa inputan
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
  system("clear");

  char A = ' ';

  printf("Masukkan Suatu Karakter : ");scanf("%c",&A);  
  if (A=='A')
  {
    printf("Anda menekan A besar\n"); 
  }else {
    printf("Anda tidak menekan A besar\n");
  }

  return 0;
}