/*
   Nama Program : bintang.c
   Tgl buat     : 7 November 2023
   Deskripsi    : mencetak bintang
*/

#include <stdio.h>
#include <stdlib.h>

int main()
{
  system("clear");

  int N=0;  // Number of rows

    printf("Masukkan jumlah awal bintang : ");
    scanf("%d", &N);

    for (int i = N; i >= 0; i--) {
        for (int j = 1; j <= i; j++) {
            printf("*");
        }
        printf("\n");
    } 

  return 0;
}